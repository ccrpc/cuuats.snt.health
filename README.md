# CUUATS Health Impact Assessment
[CUUATS][1] aims to increase understanding about the different ways the
[transportation system is connected to population health][2]. One of the most
widely documented impacts of transportation infrastructure on health outcomes
is manifested through individual travel mode choices as they are related to
physical activity. People who routinely walk and bike are more likely to get
the recommended levels of daily physical activity for good health, which can
lower the risk of certain health conditions such as obesity, hypertension, and
type II diabetes.

In 2014, CUUATS staff published a [Health Impact Assessment][3] that
established a relationship between transportation and built environment
characteristics and obesity in the community. During 2019 and 2020, the
Health Impact Assessment was integrated into the [Sustainable Neighborhoods
Toolkit][4]. The assessment was updated to draw on score data from
[Access Score][5] instead of drawing directly on characteristics of the
transportation system. The access scores represent how easy or
difficult it is to access ten common types of destinations using four modes
of transportation. The Health Impact Assessment uses the bicycle and
pedestrian access scores, since these modes of transportation are associated
with increased physical activity.

CUUATS obtained aggregated data from a major health provider about the number
of patients treated for obesity, hypertension, and type II diabetes between
2013 and 2017 in the Champaign Urbana Urbanized Area. Using these data, CUUATS
staff created a regression model that associates the number of patients with 
the average bicycle and pedestrian access score in each traffic analysis zone
for the 2015 Baseline scenario created as part of the [Champaign-Urbana 
Long Range Transportation Plan (LRTP) 2045][6]. Controlling for race and age, 
the model showed a significant inverse relationship between pedestrian and 
bicycle accessibility and the number of patients treated for these diseases.

## Evaluating Future Scenarios
While the relationship between bicycle and pedestrian accessibility and 
health does not establish causation, scientific evidence suggests that even
small increases in active transportation could lead to significant decreases in
chronic disease. As such, it is appropriate to explore the possible impact
of future transportation projects on population health.

CUUATS staff applied the model from the Health Impact Assessment to access
scores from the Champaign-Urbana LRTP [2045 Preferred Scenario][7]. This
scenario reflects ambitious implementation of bike and pedestrian
recommendations in current plans, projected transit system changes, future
environmental considerations and actions, and an emphasis on infill (over
peripheral or sprawl) development.

Preliminary modeling suggests that bicycle and pedestrian accessibility
improvements in the 2045 Preferred scenario could lead to 292 fewer patients
(a 1.7% reduction) requiring treatment for obesity, hypertension, and
type II diabetes per year compared to the 2015 Baseline scenario. These
predicted reductions do not take into consideration population growth.

## Data
The Health Impact Assessment draws on four primary sources of data:

- **Traffic analysis zone (TAZ) boundaries:** TAZs serve as the geographic
  unit of analysis.
- **Aggregated health data:** The annual number of patients treated for 
  [diagnoses associated with obesity, hypertension, and type II diabetes][8]
  in each TAZ were provided by a major health provider.
- **Access scores:** Bicycle and pedestrian access scores for each intersection
  in the study area were generated using the [CUUATS Accessibility
  Assessment][9].
- **Demographics:** 5-Year block group estimates from the American Community
  Survey were downloaded using the Census API.

The demographic variables used as covariates in the regression model are:

- Median age
- Percent Black or African American population
- Percent Asian population

Other demographic variables were investigated but rejected because they
were not significant, or because they showed the opposite of the
relationship to health outcomes predicted in the literature:

- Per-capita income
- Vehicles per household
- Percent below the poverty level
- Percent unemployed
- Percent uninsured
- Percent with less than a bachelor’s degree

The demographic variables were redistributed from block groups to TAZs using
an area-weighted average. The mean combined bicycle and pedestrian access score
and mean number of patients per year was also calculated for each TAZ.

## Study Area
The study area for the analysis was created using TAZs meeting these criteria:

- Population of at least 100
- "Urban" TAZ type

Some TAZs in the University of Illinois campus area were removed from the study
area since students living in these TAZs have different demographic
characteristics than the rest of the community and receive treatment at the 
University's medical facilities.

## Methods and Results
The Health Impact Assessment regression is based on a Poisson distribution and
uses population as an offset. The Poisson regression type was selected
because the data are non-negative, discrete counting data that would not be
well-fitted using a normal OLS model. Some variables were logarithmically
transformed to improve the fit of the model.

### Correlation Matrix
A correlation matrix generated from the model shows a negative
correlation between total count and access score.

![A matrix showing the correlations between patients, access score, median age,
percent Asian population, and percent Black or African American population](
./images/correlation-plot.png)

### Regression Summary
The regression summary shows that access score, the log of median age, 
percent Asian population, and percent Black or African American population
are all significant predictors of total health problem counts.

```
Call:
glm(formula = patients ~ score + log(acs_median_age) + acs_black_percent + 
    acs_asian_percent, family = poisson(link = log), data = scenarios[[scenario.names[1]]], 
    offset = log(population))

Deviance Residuals: 
     Min        1Q    Median        3Q       Max  
-12.0667   -2.2538   -0.2064    1.9352   13.1819  

Coefficients:
                      Estimate Std. Error z value Pr(>|z|)    
(Intercept)         -5.5412371  0.1525001 -36.336   <2e-16 ***
score               -0.0065031  0.0004796 -13.560   <2e-16 ***
log(acs_median_age)  1.0968062  0.0386724  28.361   <2e-16 ***
acs_black_percent    0.4065756  0.0485277   8.378   <2e-16 ***
acs_asian_percent   -1.5136852  0.1020813 -14.828   <2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

(Dispersion parameter for poisson family taken to be 1)

    Null deviance: 3511.1  on 109  degrees of freedom
Residual deviance: 1673.6  on 105  degrees of freedom
AIC: Inf

Number of Fisher Scoring iterations: 4
```

### Actual vs. Predicted Values
The number of annual patients in each TAZ was predicted using the model and
plotted against the actual count. The model has an R-squared value of 0.8431.

![A plot with the actual annual patients on the x-axis and predicted
annual patients on the y-axis](./images/actual-predicted.png)

## Running the Assessment
The Health Impact Assessment is implemented using the [`R` statistical
programming language][10]. To run the assessment, follow the steps below.

### Prepare Data
The input data sets should be added to the GeoPackage specified in the
`input.gpkg` configuration variable.

#### Traffic Analysis Zones
The input GeoPackage should contain a polygon layer named `taz`. The layer
should include one feature for each traffic analysis zone included in the
analysis. The layer should include these attributes:

- `id` (integer): Unique identifier for the TAZ.
- `population` (integer): Population of the TAZ.
- `patients` (integer): Number of patients treated annually for the selected
  conditions who live in the TAZ.

#### Scenario Scores
For each scenario in the `scenario.names` configuration variable, the input
GeoPackage should include a point layer of the same name. The points represent
street network intersections. Each layer should have attributes representing
bicycle and pedestrian access scores. The score attribute names should begin
with `bicycle_` and `pedestrian_` respectively. These scores can be generated
using the [CUUATS Accessibility Assessment][9].

### Install Dependencies
The Health Impact Assessment depends on statistics, GIS, and Census data packages 
from CRAN. Depending on the platform, some of these packages may require
additional headers or binary libraries to compile:

```R
install.packages('corrplot')
install.packages('faraway')
install.packages('ggplot2')
install.packages('raster')
install.packages('rgdal')
install.packages('rgeos')
install.packages('tidycensus')
install.packages('tidyverse')
```

### Save Census API Key
The Health Impact Assessment downloads data from the American Community
Survey using the Census API. Accessing the Census API requires [an API key][11],
which must be saved in the R environment:

```R
library(tidycensus)
census_api_key('MY_API_KEY', install=TRUE)
readRenviron('~/.Renviron')
```

### Set Data Directory
Before running the assessment, set the working directory to the directory
containing the input data GeoPackage:

```R
setwd('/path/to/data')
```

### Customize Configuration
The first section of the `health_impact_assessment.R` script contains
configuration variables that define the input and output file names, the names
of the scenarios to be evaluated, and the American Community Survey demographic
variables that serve as covariates. These variables can be changed to customize
the behavior of the assessment.

### Run the Assessment Script
Running the `health_impact_assessment.R` script downloads demographic data
from the American Community Survey, reaggregates demographic characteristics
to the traffic analysis zones, and calculates the average bicycle and
pedestrian access score for each TAZ in each scenario. It then fits a
regression model using the baseline scenario and uses it to predict the
number patients for each scenario. The TAZ predictions for each scenario
are written to the output GeoPackage, and a summary of the total number of
predicted patients in each scenario is printed.

## Future Work
The regression model used in the Health Impact Assessment is a simple linear
model designed to establish the relationship between bicycle and
pedestrian accessibility and incidence of chronic disease. Future work could
improve this model to better predict health outcomes:

- The linear model does not account for spatial autocorrelation in the
  bicycle and pedestrian access scores. A spatial autoregressive model
  could better control for spatial relationships in the input data.

- The current model uses race and age as covariates but does not include
  other demographic variable known to influence health, such as income.
  Future modeling could include these variables.

- Patients treated for obesity, hypertension, and type II diabetes are treated
  as a single group in the model. In the future, these conditions could be
  modeled separately to examine whether each disease has a different
  relationship with bicycle and pedestrian accessibility.

## License
The CUUATS Health Impact Assessment is available under the terms of the
[BSD 3-clause license][12].


[1]: https://ccrpc.org/programs/transportation/
[2]: https://ccrpc.gitlab.io/lrtp2045/existing-conditions/health/
[3]: https://ccrpc.org/documents/health-impact-assessment/
[4]: https://ccrpc.org/documents/sustainable-neighborhoods-toolkit/
[5]: https://ccrpc.gitlab.io/access-score/
[6]: https://ccrpc.gitlab.io/
[7]: https://ccrpc.gitlab.io/lrtp2045/vision/model/
[8]: https://gitlab.com/ccrpc/cuuats.snt.health/-/tree/master/diagnosis
[9]: https://gitlab.com/ccrpc/cuuats.snt.accessibility
[10]: https://www.r-project.org/
[11]: https://api.census.gov/data/key_signup.html
[12]: https://gitlab.com/ccrpc/cuuats.snt.health/-/blob/master/LICENSE.md
